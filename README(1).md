# barquette

Le client (Barquette & Cie) se charge de contacter des producteurs locaux afin qu'ils fournissent des fruits conditionnés à destination des écoles locales (du côté de Montauban).     
Il leur faut un site web responsive afin de présenter leur activité (voir documents joints).

## Consignes
* A rendre avant ce soir minuit
* Travail en individuel
* Pas de CMS, que du front-end (pas de Node.js).
* HTML/CSS (et JS si besoin).

## Livrables attendus
* Wireframes (desktop/tablette/mobile)
* Code source du site sous la forme d'un repo gitlab
* Version en ligne du site

## Critères d'évaluation
Les compétences évaluées ici sont les compétences 1 et 2 du référentiel DWWM. 
Les voici ainsi que les critères de performance associés. 

### Compétence 1 : maquetter une application
* La maquette prend en compte les spécificités fonctionnelles décrites dans les cas d'utilisation ou les
scénarios utilisateur
* L'enchaînement des écrans est formalisé par un schéma
* La maquette et l'enchaînement des écrans sont validés par l’utilisateur
* La maquette respecte la charte graphique de l’entreprise
* La maquette est conforme à l'expérience utilisateur et à l’équipement ciblé
* La maquette respecte les principes de sécurisation d’une interface utilisateur
* La maquette prend en compte les exigences de sécurité spécifiques de l’application
* Le contenu de la maquette est rédigé, en français ou en anglais, de façon adaptée à l’interlocuteur et sans
faute

### Compétence 2 : Réaliser une interface utilisateur web statique et adaptable
* L'interface est conforme à la maquette de l'application
* Les pages web respectent la charte graphique de l'entreprise
* Les bonnes pratiques de structuration et de sécurité sont respectées y compris pour le web mobile
* Les pages web sont accessibles depuis les navigateurs ciblés y compris depuis un mobile
* Les pages web s’adaptent à la taille de l’écran
* Les pages web sont optimisées pour le web mobile
* Le site respecte les règles de référencement naturel
* Les pages web sont publiées sur un serveur
* L’objet de la recherche est exprimé de manière précise en langue française ou anglaise
* La documentation technique liée aux technologies associées, en français ou en anglais, est comprise
(sans contre-sens, ...)
* La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une
nouvelle fonctionnalité
* Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs


## Suites de l'évaluation
Suite à cette évaluation (qui n'est bien sûr pas notée), les projets ainsi produits seront revus par les formateurs en compagnie de chaque apprenant. L'objectif est de déterminer l'avancement de l'apprenant ainsi que les éventuels axes d'amélioration à envisager. Le but n'est pas de sanctionner ou critiquer la progression mais de discuter de son avancement afin de poser ensemble tout ce qui est nécessaire pour le bon déroulement de la formation. Notamment en fonction des moyens d'apprentissage propres à chacun.   
L'équipe pédagogique est là pour rassurer et accompagner, pas pour sanctionner. 

## Bonus 
Sans partir dans la réalisation de cette démarche, comment assureriez-vous l'éco-conception de ce site?   
Selon vous, quelles sont 3 à 5 actions à privilégier (techniques ou non)?
